﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Security.Claims;
using System.Web;
using RiversideHealth.Models;

//Referenced Christine's code

namespace RiversideHealth.Data
{
    //Referenced Christine's code
    public class RiversideHealthDbContext : IdentityDbContext<ApplicationUser>
    {
        public RiversideHealthDbContext(DbContextOptions<RiversideHealthDbContext> options)
        : base(options)
        {

        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Announcement> Announcements { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Career> Careers { get; set; }

        public DbSet<Donator> Donators { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<GiftItems> GiftItems { get; set; }
        public DbSet<GiftCategory> GiftCategories { get; set; }
        public DbSet<Locations> Locations { get; set; }
        public DbSet<HealthTips> HealthTips { get; set; }
        public DbSet<HealthTipsCategory> HealthTipsCategories { get; set; }
        public DbSet<Slider> Sliders { get; set; }
        public DbSet<ProgramServices> ProgramServices { get; set; }
        public DbSet<News> News { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Admin has many pages, each page has one author
            modelBuilder.Entity<Page>()
                .HasOne(a => a.Admin)
                .WithMany(p => p.Pages)
                .HasForeignKey(p => p.AdminId);

            //Admin has many announcements, each announcement has one author.
            modelBuilder.Entity<Announcement>()
                .HasOne(a => a.Admin)
                .WithMany(a => a.Announcements)
                .HasForeignKey(a => a.AdminId);

            //One admin has one application user and that user has one admin
            modelBuilder.Entity<Admin>()
                .HasOne(a => a.User)
                .WithOne(u => u.Admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminId);

            // donator has many transactions, transaction has one donator 
            modelBuilder.Entity<Donator>()
                .HasMany(t => t.Transactions)
                .WithOne(d => d.Donator);

            // transaction has one donator
            modelBuilder.Entity<Transaction>()
                .HasOne(d => d.Donator)
                .WithMany(t => t.Transactions)
                .HasForeignKey(d => d.DonatorId);

            // one admin can manage many transactions
            modelBuilder.Entity<Transaction>()
                .HasOne(a => a.Admin)
                .WithMany(t => t.Transactions)
                .HasForeignKey(a => a.AdminId);

            // Gift Items has one Gift Category , Gift Category has many gift items
            modelBuilder.Entity<GiftItems>()
                .HasOne(gi => gi.GiftCategory)
                .WithMany(gi => gi.GiftItems)
                .HasForeignKey(gi => gi.GiftCategoryId);

            // Gift Categories has many items, Gift items has one category
            modelBuilder.Entity<GiftCategory>()
                .HasMany(gi => gi.GiftItems)
                .WithOne(gc => gc.GiftCategory);

            modelBuilder.Entity<Locations>()
                .HasMany(l => l.Giftitems_locations)
                .WithOne(l => l.Locations);

            // many locations has many giftitems, 
            modelBuilder.Entity<GiftItems_Locations>()
                .HasKey(gl => new { gl.GiftItemsId, gl.LocationsId });

            modelBuilder.Entity<GiftItems_Locations>()
                .HasOne(gl => gl.GiftItems)
                .WithMany(gl => gl.Giftitems_locations)
                .HasForeignKey(gl => gl.GiftItemsId);

            modelBuilder.Entity<GiftItems_Locations>()
                .HasOne(gl => gl.Locations)
                .WithMany(gl => gl.Giftitems_locations)
                .HasForeignKey(gl => gl.LocationsId);

            modelBuilder.Entity<HealthTips>()
                .HasOne(ht => ht.HealthTipsCategory)
                .WithMany(ht => ht.HealthTips)
                .HasForeignKey(ht => ht.TipsCategoryId);

            modelBuilder.Entity<HealthTipsCategory>()
                .HasMany(htc => htc.HealthTips)
                .WithOne(htc => htc.HealthTipsCategory);

            modelBuilder.Entity<News>()
               .HasOne(a => a.Admin)
               .WithMany(n => n.News)
               .HasForeignKey(n => n.AdminId);

            modelBuilder.Entity<ProgramServices>()
                .HasOne(a => a.Admin)
                .WithMany(s => s.ProgramServices)
                .HasForeignKey(n => n.AdminId);



            //Make tables
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Admin>().ToTable("Admins");
            modelBuilder.Entity<Announcement>().ToTable("Announcements");
            modelBuilder.Entity<Page>().ToTable("Pages");
            modelBuilder.Entity<Career>().ToTable("Careers");

            // Create table
            modelBuilder.Entity<Donator>().ToTable("Donators");
            modelBuilder.Entity<Transaction>().ToTable("Transactions");
            modelBuilder.Entity<GiftItems>().ToTable("GiftItems");
            modelBuilder.Entity<GiftCategory>().ToTable("GiftCategories");
            modelBuilder.Entity<GiftItems_Locations>().ToTable("GiftLocations");
            modelBuilder.Entity<Locations>().ToTable("Locations");
            modelBuilder.Entity<HealthTips>().ToTable("HealthTips");
            modelBuilder.Entity<HealthTipsCategory>().ToTable("HealthTipsCategories");
            modelBuilder.Entity<Slider>().ToTable("Sliders");
            modelBuilder.Entity<News>().ToTable("News");
            modelBuilder.Entity<ProgramServices>().ToTable("ProgramServices");

        }

        public DbSet<RiversideHealth.Models.GiftItems_Locations> GiftItems_Locations { get; set; }
    }
}