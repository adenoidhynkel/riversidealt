﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace RiversideHealth.Controllers
{
    public class GiftShopController : Controller
    {
        private readonly RiversideHealthDbContext context;

        public GiftShopController(RiversideHealthDbContext ctx)
        {
            context = ctx;
        }

        public ActionResult Index()
        {
            return View(context.GiftItems.ToList());
        }

        public ActionResult Details(int id)
        {
            return View(context.GiftItems.Find(id));
        }

        public ActionResult Create()
        {
            // create gift category entity
            GiftItemEdit ge = new GiftItemEdit();
            ge.GiftCategories = context.GiftCategories.ToList();
            
            // return the view
            return View(ge);
        }

        public ActionResult Edit(int id)
        {
            return View(context.GiftItems.Find(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(string GiftItemName_New, string GiftItemDescription_New, decimal GiftItemPrice_New, int GiftCategoryId_New)
        {

            GiftItems item = new GiftItems()
            {
                GiftItemName = GiftItemName_New,
                GiftItemDescription = GiftItemDescription_New,
                GiftItemPrice = GiftItemPrice_New,
                GiftCategoryId = GiftCategoryId_New
            };

            //Debug.WriteLine("Price: " + item.GiftItemPrice);
            //insert query
            context.GiftItems.Add(item);

            //save
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, string GiftItemName_Update, string GiftItemDescription_Update, decimal GiftItemPrice_Update, int GiftCategoryId_Update, IFormFile authorimg)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            GiftItems item = new GiftItems()
            {
                GiftItemsId = id
            };
            
            // attach to entity
            context.GiftItems.Attach(item);

            // add the update
            

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Details/" + id);
        }

        public ActionResult Delete(int id)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select

            GiftItems item = new GiftItems() { GiftItemsId = id };

            // attach to entity
            context.GiftItems.Attach(item);

            // remove the record
            context.GiftItems.Remove(item);

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}