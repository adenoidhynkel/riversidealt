﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.EntityFrameworkCore;
//using System.Data.SqlClient;
//using RiversideHealth.Models;
//using RiversideHealth.Models.ViewModels;
//using RiversideHealth.Data;
//using Microsoft.AspNetCore.Http;
//using System.Diagnostics;

//namespace RiversideHealth.Controllers
//{

//    namespace RiversideHealth.Controllers
//    {
//        public class NewsController : Controller
//        {
//            private readonly RiversideHealthDbContext db;
//            private readonly ActionResult _env;
//            private readonly UserManager<ApplicationUser> _userManager;
//            private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

//            public NewsController(RiversideHealthDbContext context, ActionResult env, UserManager<ApplicationUser> usermanager)
//            {
//                db = context;
//                _env = env;
//                _userManager = usermanager;
//            }
//            public async Task<ActionResult> Index()
//            {
//                List<News> news = await db.News.Include(a => a.Admin).ToListAsync();

//                var user = await GetCurrentUserAsync();
//                if (user != null)
//                {
//                    if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
//                    else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
//                    return View(news);
//                }
//                else
//                {
//                    ViewData["UserHasAdmin"] = "None";
//                    return View(newsEditView);
//                }
//            }

//        }

//    }
//}