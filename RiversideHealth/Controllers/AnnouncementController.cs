﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

//References Christine's blog controller.

namespace RiversideHealth.Controllers
{
    public class AnnouncementController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AnnouncementController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }

        //Pagination algorithm from Christine's code
        public async Task<ActionResult> Index(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            /*Pagination Algorithm*/

            var _announcements = await db.Announcements.Include(a => a.Admin).ToListAsync();
            int pagecount = _announcements.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Announcement> announcements = await db.Announcements.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(announcements);


        }

        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            AnnouncementEdit announcementEditView = new AnnouncementEdit();
            announcementEditView.Admins = db.Admins.ToList();
            return View(announcementEditView);
        }

        [HttpPost]
        public ActionResult Create(string AnnouncementTitle_New, string AnnouncementContent_New, int AnnouncementAuthor_New, int AnnouncementDisplayStatus)
        {

            string query = "insert into Announcements (Date, Title, Content, DisplayStatus, AdminId) values (@date, @title, @content, @displayStatus, @admin)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@date", DateTime.UtcNow);
            myparams[1] = new SqlParameter("@title", AnnouncementTitle_New);
            myparams[2] = new SqlParameter("@content", AnnouncementContent_New);
            myparams[3] = new SqlParameter("@displayStatus", AnnouncementDisplayStatus);
            myparams[4] = new SqlParameter("@admin", AnnouncementAuthor_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Details(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            return View(db.Announcements.Include(a => a.Admin).SingleOrDefault(p => p.AnnouncementId == id));
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from announcements where announcementid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("Index");
        }


    }


}