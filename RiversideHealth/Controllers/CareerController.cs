﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Models.ViewModels;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;

namespace RiversideHealth.Controllers
{
    public class CareerController : Controller
    {
        private readonly RiversideHealthDbContext db;
        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public CareerController(RiversideHealthDbContext context, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _userManager = usermanager;
        }
        public async Task<ActionResult> AdminIndex(int pagenum)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            /*Pagination Algorithm*/
            var _careers = await db.Careers.Include(a => a.Admin).ToListAsync();
            int pagecount = _careers.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Career> careers = await db.Careers.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(careers);
        }

        public async Task<ActionResult> PublicIndex(int pagenum)
        {
            /*Pagination Algorithm*/
            var _careers = await db.Careers.Include(a => a.Admin).ToListAsync();
            int pagecount = _careers.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)pagecount / perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if (maxpage > 0)
            {
                ViewData["PaginationSummary"] =
                    (pagenum + 1).ToString() + " of " +
                    (maxpage + 1).ToString();
            }
            List<Career> careers = await db.Careers.Include(a => a.Admin).Skip(start).Take(perpage).ToListAsync();
            /*End of Pagination  Algorithm*/

            return View(careers);
        }


        public async Task<ActionResult> Create()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }

            CareerEdit careerEditView = new CareerEdit();
            careerEditView.Admins = db.Admins.ToList();
            return View(careerEditView);
        }


        [HttpPost]
        public ActionResult Create(string RoleName_New, int RoleType, string RoleDescription_New, int CareerAuthor_New, float Wage_New, int RoleStatus, string ContactPerson_New, string ContactPersonEmail_New)
        {

            string query = "insert into Careers (Date, CareerName, CareerType, CareerDescription, Status, Wage, AdminId, ContactEmail, ContactName) values (@date, @rolename, @type, @description, @status, @wage, @admin, @email, @contact)";
            SqlParameter[] myparams = new SqlParameter[9];
            myparams[0] = new SqlParameter("@date", DateTime.UtcNow);
            myparams[1] = new SqlParameter("@rolename", RoleName_New);
            myparams[2] = new SqlParameter("@type", RoleType);
            myparams[3] = new SqlParameter("@description", RoleDescription_New);
            myparams[4] = new SqlParameter("@status", RoleStatus);
            myparams[5] = new SqlParameter("@wage", RoleStatus);
            myparams[6] = new SqlParameter("@admin", CareerAuthor_New);
            myparams[7] = new SqlParameter("@email", ContactPersonEmail_New);
            myparams[8] = new SqlParameter("@contact", ContactPerson_New);



            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("AdminIndex");
        }


        public async Task<ActionResult> AdminDetails(int id)
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminId == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminId.ToString(); }
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
            }
            return View(db.Careers.Include(a => a.Admin).SingleOrDefault(p => p.CareerId == id));
        }

        public ActionResult PublicDetails(int id)
        {
            return View(db.Careers.SingleOrDefault(p => p.CareerId == id));
        }

        public ActionResult Delete(int id)
        {
            string query = "delete from careers where careerid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("AdminIndex");
        }

    }
}