﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using RiversideHealth.Models;
using RiversideHealth.Data;
using Microsoft.AspNetCore.Http;

namespace RiversideHealth.Controllers
{
    public class GiftCategoryController : Controller
    {
        private readonly RiversideHealthDbContext context;

        public GiftCategoryController(RiversideHealthDbContext ctx)
        {
            context = ctx;
        }

        public ActionResult Index()
        {
            return View(context.GiftCategories.ToList());
        }

        public ActionResult Details(int id)
        {
            return View(context.GiftCategories.Find(id));
        }

        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            return View(context.GiftCategories.Find(id));
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("GiftCategoryName")] GiftCategory gc)
        {
            //insert query
            context.GiftCategories.Add(gc);

            //save
            context.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, string GiftCategoryName_Update)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            GiftCategory gc = new GiftCategory() { GiftCategoryId = id };
            
            // attach to entity
            context.GiftCategories.Attach(gc);

            // add the update
            gc.GiftCategoryName = GiftCategoryName_Update;

            // save the changes
            context.SaveChanges();

           return RedirectToAction("Details/" + id);
        }


        public ActionResult Delete(int id)
        {
            //create new object
            // reference https://davidsekar.com/c-sharp/modify-or-delete-entity-object-without-select
            GiftCategory gc = new GiftCategory() { GiftCategoryId = id };

            // attach to entity
            context.GiftCategories.Attach(gc);

            // remove the record
            context.GiftCategories.Remove(gc);                                                                              

            // save the changes
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}