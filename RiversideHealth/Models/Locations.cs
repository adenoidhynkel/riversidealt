﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class Locations
    {
        [Key]
        public int LocationId { get; set; }

        [Required, StringLength(50), Display(Name = "Location Name")]
        public string LocationName { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Location Hours")]
        public string LocationHours { get; set; }

        public virtual ICollection<GiftItems_Locations> Giftitems_locations { get; set; }
    }
}
