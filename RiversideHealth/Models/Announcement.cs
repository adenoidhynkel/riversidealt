﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace RiversideHealth.Models
{
    public class Announcement
    {
        [Key]
        public int AnnouncementId { get; set; }

        [Required, DataType(DataType.DateTime), Display(Name = "Date")]
        public string Date { get; set; }

        [Required, StringLength(50), Display(Name = "Announcement Title")]
        public string Title { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Announcement Content"),]
        public string Content { get; set; }

        //0 for undisplayed items, 1 for displaying item(s).
        [Required, Display(Name = "Display Status")]
        public int DisplayStatus { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }

        public virtual Admin Admin { get; set; }

    }
}
