﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace RiversideHealth.Models
{
    public class ProgramServices
    {
            [Key]
            public int ProgramServicesid { get; set; }

            [Required, StringLength(50), Display(Name = "Service Name")]
            public string ProgramServicesName { get; set; }

            [Required, StringLength(50), Display(Name = "Service Hours")]
            public string ProgramServicesHours { get; set; }

            [ForeignKey("AdminId")]
            public int AdminId { get; set; }

            public virtual Admin Admin { get; set; }


    }
}
