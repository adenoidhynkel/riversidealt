﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

//Referenced Christine's code for Authors.
namespace RiversideHealth.Models
{
    public class Admin
    {
        [Key]
        public int AdminId { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string FName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string LName { get; set; }

        //Reference ASPNET Users
        [ForeignKey("UserID")]
        public string UserId { get; set; }

        //One admin to one application user relationship
        public virtual ApplicationUser User { get; set; } 

        //One Admin to many pages relationship
        [InverseProperty("Admin")]
        public virtual List<Page> Pages { get; set; }

        //One Admin to many announcements relationship
        [InverseProperty("Admin")]
        public virtual List<Announcement> Announcements { get; set; }

        [InverseProperty("Admin")]
        public virtual List<Career> Careers { get; set; }

        [InverseProperty("Admin")]
        public virtual List<Transaction> Transactions { get; set; }

        //One Admin to many News relationship
        [InverseProperty("Admin")]
        public virtual List<News> News { get; set; }

        //One Admin to many services relationship
        [InverseProperty("Admin")]
        public virtual List<ProgramServices> ProgramServices { get; set; }
    }
}

    