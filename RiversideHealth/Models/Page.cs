﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

//Referenced model codes from Christine's example
namespace RiversideHealth.Models
{
    public class Page
    {
        [Key]
        public int PageId { get; set; }

        [Required, StringLength(50), Display(Name = "Page Title")]
        public string Title { get; set; }

        //https://stackoverflow.com/questions/5252979/assign-format-of-datetime-with-data-annotations
        [Required, DataType(DataType.DateTime), Display(Name = "Date Created")]
        public DateTime Date { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Page Content")]
        public string Content { get; set; }

        //0 for unpublished, 1 for publish.
        [Required, Display(Name = "Publish Status")]
        public int PublishStatus { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }
        //One to many relationship. One page to many authors.
        public virtual Admin Admin { get; set; }
    }
}
