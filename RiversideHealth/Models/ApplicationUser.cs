﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;


//References from Christine's ApplicationUser model.
namespace RiversideHealth.Models
{
 public class ApplicationUser : IdentityUser
    {
        //Configure one to one relationship between admin and and the user
        [ForeignKey("AdminId")]
        public int? AdminId { get; set; }

        //An Application User is the admin.
        public virtual Admin Admin { get; set; }

    }
}