﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models
{
    public class DonationSummary
    {
        public string CustID { get; set; }
        public string CustFullName { get; set; }
        public string CustEmail { get; set; }
        public string DonationReceiptLink { get; set; }
        public string ReferenceID { get; set; }
        public string Status { get; set; }
        public string Amount { get; set; }
    }
}
