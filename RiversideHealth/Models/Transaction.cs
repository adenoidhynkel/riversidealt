﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class Transaction
    {
        [Key]
        public string TransactionId { get; set; }

        [Required, Column(TypeName = "decimal(10, 2)"), Display(Name = "Transaction Amount")]
        public decimal TransactionAmount { get; set; }

        [Required, StringLength(255), Display(Name = "Reference ID")]
        public string ReferenceId { get; set; }

        //https://stackoverflow.com/questions/5252979/assign-format-of-datetime-with-data-annotations
        [Required, DataType(DataType.DateTime), Display(Name = "Transaction Date")]
        public DateTime TransactionDate { get; set; }

        [Required, StringLength(20), Display(Name = "Transaction Status")]
        public string TransactionStatus { get; set; }

        [ForeignKey("AdminId")]
        public int AdminId { get; set; }

        public virtual Admin Admin { get; set; }

        [ForeignKey("DonatorId")]
        public int DonatorId { get; set; }

        public virtual Donator Donator { get; set; }
    }
}
