﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class GiftCategory
    {
        [Key]
        public int GiftCategoryId { get; set; }

        [Required, StringLength(255), Display(Name = "Category Name")]
        public string GiftCategoryName { get; set; }

        [InverseProperty("GiftCategory")]
        public virtual List<GiftItems> GiftItems { get; set; }
    }
}
