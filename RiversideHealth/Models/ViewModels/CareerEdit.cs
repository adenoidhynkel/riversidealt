﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models.ViewModels
{
    public class CareerEdit
    {
        public CareerEdit()
        {

        }
        public virtual Career Career { get; set; }

        public IEnumerable<Admin> Admins { get; set; }
    }

}
