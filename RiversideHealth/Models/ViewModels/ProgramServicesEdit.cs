﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RiversideHealth.Models.ViewModels
{
    public class ProgramServicesEdit
    {
        public ProgramServicesEdit()
        {

        }

        public virtual ProgramServices ProgramServices { get; set; }

        public IEnumerable<Admin> Admins { get; set; }
    }

    
}
