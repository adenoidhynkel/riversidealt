﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RiversideHealth.Models
{
    public class GiftItems
    {
        [Key]
        public int GiftItemsId { get; set; }

        [Required, StringLength(50), Display(Name = "Item Name")]
        public string GiftItemName { get; set; }

        [Required, StringLength(255), Display(Name = "Item Description")]
        public string GiftItemDescription { get; set; }

        [Required, Column(TypeName = "decimal(10, 2)"), Display(Name = "Item Price")]
        public decimal GiftItemPrice { get; set; }

        // set default values
        // reference: https://stackoverflow.com/questions/23823103/default-value-in-mvc-model-using-data-annotation
        // author: Nilesh, Jeppe Stig Nelsen

        [Required, StringLength(255), Display(Name = "Item Image")]
        public string GiftItemImage { get; set; } = "Default.jpg";

        //0 for True, 1 for False.
        public int HasImages { get; set; } = 0;

        [ForeignKey("GiftCategoryId")]
        public int GiftCategoryId { get; set; }
    
        public virtual GiftCategory GiftCategory { get; set; }

        // one giftitem to many locations
        [InverseProperty("GiftItems")]
        public virtual List<GiftItems_Locations> Giftitems_locations { get; set; }

    }
}
